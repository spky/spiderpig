from flask import Flask

from spiderpig.base import CSRF, DB, LMAN
from spiderpig.fixed import ERROR_CODES
from spiderpig.shared import bookmarklet, error_handler, favicon, mark, moment
from spiderpig.views.crawl import BLUEPRINT_CRAWL
from spiderpig.views.main import BLUEPRINT_MAIN
from spiderpig.views.side import BLUEPRINT_SIDE
from spiderpig.views.user import BLUEPRINT_USER


def create_app(conf_obj):
    app = Flask(__name__.split('.')[0])
    app.config.from_object(conf_obj)

    _register_extensions(app)
    _register_errorhandlers(app)
    _register_favicon(app)
    _register_blueprints(app)
    _register_filters(app)
    _register_methods(app)

    _initialize_db(app)
    _initialize_lman(app)

    return app


def _register_extensions(app):
    DB.init_app(app)
    CSRF.init_app(app)
    LMAN.init_app(app)


def _register_errorhandlers(app):
    for code in ERROR_CODES:
        app.errorhandler(code)(error_handler)


def _register_favicon(app):
    app.add_url_rule('/favicon.ico', view_func=favicon)


def _register_blueprints(app):
    app.register_blueprint(BLUEPRINT_MAIN)
    app.register_blueprint(BLUEPRINT_USER)
    app.register_blueprint(BLUEPRINT_SIDE)
    app.register_blueprint(BLUEPRINT_CRAWL)


def _register_filters(app):
    app.template_filter('moment')(moment)
    app.template_filter('mark')(mark)


def _register_methods(app):
    app.jinja_env.globals.update(bookmarklet=bookmarklet)


def _initialize_db(app):
    DB.create_all(app=app)


def _initialize_lman(_):
    LMAN.login_view = 'user.login'
    LMAN.session_protection = 'strong'
