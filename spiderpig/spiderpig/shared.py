from datetime import datetime, timezone

from flask import render_template, send_from_directory, url_for
from jinja2 import Markup


def error_handler(error):
    return render_template(
        'shared/error.html',
        error=error
    ), error.code


def favicon():
    return send_from_directory(
        'static', 'ico/favicon.ico',
        mimetype='image/vnd.microsoft.icon'
    )


def moment(value):
    if not value or not isinstance(value, datetime):
        value = datetime.utcnow()

    value = value.replace(tzinfo=timezone.utc)
    return Markup('''
<span class="moment" title="{title}" data-stamp="{stamp}">{title}</span>
    '''.format(
        title='{} UTC'.format(value),
        stamp=int(value.timestamp())
    ).strip())


def mark(value, term):
    term = term.strip()
    return Markup(str(value).replace(
        term, '<mark>{term}</mark>'.format(term=term)
    ))


def bookmarklet():
    return Markup(
        ' '.join(src.strip() for src in '''
javascript:(
    function() {{
        window.location.assign(
            '{FORM_URL}'.concat(
                '?address=',
                encodeURIComponent( window.location ),
                '&ttl=2',
            )
        );
    }}
)();
        '''.format(
            FORM_URL=url_for('crawl.index', _external=True)
        ).strip().splitlines())
    )
