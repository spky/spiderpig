from unittest import TestCase, main

from spiderpig.crawl.loc import Location


class TestLocation(TestCase):

    def test_empty(self):
        loc = Location('')
        self.assertIsNone(loc.scheme)
        self.assertIsNone(loc.username)
        self.assertIsNone(loc.password)
        self.assertIsNone(loc.thirdlevel)
        self.assertIsNone(loc.secondlevel)
        self.assertIsNone(loc.toplevel)
        self.assertIsNone(loc.port)
        self.assertIsNone(loc.path)
        self.assertIsNone(loc.query)
        self.assertIsNone(loc.fragment)

    def test_call(self):
        for inp in [
                'http://example.org',
                'https://example.org',
                'https://example.org/',
                'http://example.org:1337',
                'https://www.example.org/',
                'https://sub.www.example.org/',
                'https://user:pass@www.example.org/',
                'https://www.example.org/what/ever',
                'https://www.example.org/what/ever#anchor',
                'https://www.example.org/what/ever?some',
                'https://www.example.org/what/ever?some=thing',
                'https://www.example.org/what/ever?some=thing#anchor',
        ]:
            loc = Location(inp)
            self.assertEqual(inp, loc())

    def test_construct(self):
        loc = Location('https://example.org')
        ctx_loc = Location.construct(loc, 'some/thing')
        self.assertEqual('https://example.org/some/thing', ctx_loc())

        ctx_loc = Location.construct(ctx_loc, '/what/ever')
        self.assertEqual('https://example.org/what/ever', ctx_loc())

        ctx_loc = Location.construct(loc, 'http://www.example.com')
        self.assertEqual('http://www.example.com', ctx_loc())

    def test_scheme(self):
        loc = Location('https://example.org')
        self.assertEqual('https', loc.scheme)

        loc = Location('//example.org')
        self.assertIsNone(loc.scheme)

        loc = Location('example.org')
        self.assertIsNone(loc.scheme)

    def test_username(self):
        loc = Location('https://user@example.org')
        self.assertEqual('user', loc.username)

        loc = Location('https://user:pass@example.org')
        self.assertEqual('user', loc.username)

        loc = Location('//user:pass@example.org')
        self.assertEqual('user', loc.username)

        loc = Location('example.org')
        self.assertIsNone(loc.username)

    def test_password(self):
        loc = Location('https://user:pass@example.org')
        self.assertEqual('pass', loc.password)

        loc = Location('https://user@example.org')
        self.assertIsNone(loc.password)

        loc = Location('//user:pass@example.org')
        self.assertEqual('pass', loc.password)

        loc = Location('example.org')
        self.assertIsNone(loc.password)

    def test_netloc(self):
        loc = Location('https://example.org')
        self.assertEqual('example.org', loc.netloc)

        loc = Location('//')
        self.assertIsNone(loc.netloc)

    def test_thirdlevel(self):
        loc = Location('https://www.example.org')
        self.assertEqual('www', loc.thirdlevel)

        loc = Location('https://what.ever.example.org')
        self.assertEqual('what.ever', loc.thirdlevel)

        loc = Location('//')
        self.assertIsNone(loc.thirdlevel)

        loc = Location('https://example.org')
        self.assertIsNone(loc.thirdlevel)

    def test_secondlevel(self):
        loc = Location('https://www.example.org')
        self.assertEqual('example', loc.secondlevel)

        loc = Location('https://example.org')
        self.assertEqual('example', loc.secondlevel)

        loc = Location('//')
        self.assertIsNone(loc.secondlevel)

    def test_toplevel(self):
        loc = Location('https://example.org')
        self.assertEqual('org', loc.toplevel)

        loc = Location('https://example.com')
        self.assertEqual('com', loc.toplevel)

        loc = Location('//')
        self.assertIsNone(loc.toplevel)

    def test_port(self):
        loc = Location('https://example.org')
        self.assertIsNone(loc.port)

        loc = Location('http://example.org')
        self.assertIsNone(loc.port)

        loc = Location('//')
        self.assertIsNone(loc.port)

        loc = Location('http://telnet.example.org:23')
        self.assertEqual(23, loc.port)

        loc = Location('https://user:pass@example.org')
        self.assertIsNone(loc.port)

        loc = Location('https://user:pass@example.org:123')
        self.assertEqual(123, loc.port)

        loc = Location('fail://example.org')
        self.assertIsNone(loc.port)

        loc = Location('fail://example.org:123')
        self.assertEqual(123, loc.port)

    def test_path(self):
        loc = Location('https://example.org/whatever')
        self.assertEqual('/whatever', loc.path)

        loc = Location('https://example.org/what/ever')
        self.assertEqual('/what/ever', loc.path)

        loc = Location('https://example.org/')
        self.assertEqual('/', loc.path)

        loc = Location('https://example.org')
        self.assertIsNone(loc.path)

        loc = Location('//')
        self.assertIsNone(loc.path)

    def test_query(self):
        loc = Location('https://example.org?what=ever&some=thing')
        self.assertEqual('what=ever&some=thing', loc.query)

        loc = Location('https://example.org?what=ever&some')
        self.assertEqual('what=ever&some', loc.query)

        loc = Location('https://example.org?what&ever')
        self.assertEqual('what&ever', loc.query)

        loc = Location('https://example.org?what=ever')
        self.assertEqual('what=ever', loc.query)

        loc = Location('https://example.org?what')
        self.assertEqual('what', loc.query)

        loc = Location('https://example.org')
        self.assertIsNone(loc.query)

    def test_fragment(self):
        loc = Location('https://example.org/page#something')
        self.assertEqual('something', loc.fragment)

        loc = Location('https://example.org/page/#something')
        self.assertEqual('something', loc.fragment)

        loc = Location('https://example.org/#something')
        self.assertEqual('something', loc.fragment)

        loc = Location('https://example.org#something')
        self.assertEqual('something', loc.fragment)

        loc = Location('https://example.org')
        self.assertIsNone(loc.fragment)


if __name__ == '__main__':
    main()
