from bs4 import BeautifulSoup
from spiderpig.crawl.loc import Location
from spiderpig.models.crawl import Crawl
from spiderpig.models.url import (
    Url, UrlArgument, UrlError, UrlFragment, UrlPassword, UrlPath, UrlPort,
    UrlScheme, UrlSecondLevel, UrlStatus, UrlThirdLevel, UrlTopLevel,
    UrlTtlHop, UrlUsername,
)


class Resp:
    def __init__(
            self, *,
            loc: Location = None,
            status: int = 0,
            content: bytes = None,
            error: str = None,
    ):
        self.loc = loc if loc else Location('')
        self.status = status
        self.content = content
        self.error = error
        self._soup = None

    def __repr__(self):
        return 'Resp(loc="{}" status="{}" len="{}" err="{}")'.format(
            self.loc, self.status, len(self.content), self.error
        )

    @property
    def valid(self):
        return self.status and self.content

    @property
    def soup(self):
        if not self._soup:
            self._soup = BeautifulSoup(self.content, 'lxml')
        return self._soup

    @property
    def links(self):
        for tag, attr in [
                ('a', 'href'),
                ('link', 'href'),
                ('script', 'src'),
                ('img', 'src'),
                ('area', 'href'),
        ]:
            for elem in self.soup.find_all(tag):
                if elem.has_attr(attr):
                    yield Location.construct(self.loc, elem[attr])

    def save(
            self, *,
            crl: Crawl, ttl: int,
            parent: Url = None
    ):
        def proc(model, value):
            res = model.query_first_value(value=value)
            if not res and value is not None:
                res = model.create(value=value, _commit=True)
            return res

        return Url.create(
            crawl=crl,
            parent=parent,
            ttlhop=proc(UrlTtlHop, ttl),
            status=proc(UrlStatus, self.status),
            error=proc(UrlError, self.error),
            scheme=proc(UrlScheme, self.loc.scheme),
            username=proc(UrlUsername, self.loc.username),
            password=proc(UrlPassword, self.loc.password),
            thirdlevel=proc(UrlThirdLevel, self.loc.thirdlevel),
            secondlevel=proc(UrlSecondLevel, self.loc.secondlevel),
            toplevel=proc(UrlTopLevel, self.loc.toplevel),
            port=proc(UrlPort, self.loc.port),
            path=proc(UrlPath, self.loc.path),
            argument=proc(UrlArgument, self.loc.argument),
            fragment=proc(UrlFragment, self.loc.fragment),
            _commit=True
        )
