from spiderpig.crawl.loc import Location
from spiderpig.crawl.pull import Pull
from spiderpig.models.crawl import Crawl
from spiderpig.models.url import Url
from spiderpig.models.user import User


class Spider:
    def __init__(self):
        self._pull = None
        self._trail = set()
        self.reset()

    def reset(self):
        self._pull = None
        self._trail.clear()

    @property
    def pull(self):
        if not self._pull:
            self._pull = Pull()
        return self._pull

    def consider(self, loc: Location):
        if not loc:
            return False
        url = loc()
        if url in self._trail:
            return False
        self._trail.add(url)
        return True

    def walk(
            self, *,
            crl: Crawl, ttl: int,
            loc: Location,
            parent: Url = None,
    ):
        if not self.consider(loc):
            return

        resp = self.pull.run(loc)
        current = resp.save(crl=crl, ttl=ttl, parent=parent)
        yield current

        if resp.valid and ttl > 0:
            for link in resp.links:
                yield from self.walk(
                    crl=crl,
                    ttl=ttl - 1,
                    loc=link,
                    parent=current,
                )

    def __call__(self, *, address: str, ttl: int, user: User):
        yield from self.walk(
            crl=Crawl.create(ttl=ttl, user=user, _commit=True),
            ttl=ttl,
            loc=Location(address),
        )

        self.reset()
