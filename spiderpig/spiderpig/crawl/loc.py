from typing import Type, TypeVar

from werkzeug.urls import url_fix, url_join, url_parse, url_unparse

Lt = TypeVar('Lt', bound='Location')
class Location:
    def __init__(self, value: str):
        self._obj = None
        self._value = url_fix(value)

    @classmethod
    def generate(cls, obj: tuple):
        '''(scheme, netloc, path, query, fragment)'''
        return cls(url_unparse(obj))

    @classmethod
    def construct(cls, source: Type[Lt], target: str) -> Lt:
        if not source or '://' in target:
            return cls(target)
        return cls(url_join(source(), target))

    @property
    def obj(self):
        if self._obj is None:
            self._obj = url_parse(self._value)
        return self._obj

    def __repr__(self):
        return 'Location(url="{}")'.format(self())

    def __call__(self):
        return url_unparse(self.obj)

    @property
    def scheme(self):
        res = self.obj.scheme
        return res if res else None

    @property
    def username(self):
        return self.obj.username

    @property
    def password(self):
        return self.obj.password

    @property
    def netloc(self):
        res = self.obj.netloc
        return res if res else None

    def __level_prs(self, num):
        res = self.netloc
        if res:
            res = res.split('.')
            if len(res) >= num:
                if num >= 3:
                    return '.'.join(res[:-(num-1)])
                return res[-num]
        return None

    @property
    def thirdlevel(self):
        return self.__level_prs(3)

    @property
    def secondlevel(self):
        return self.__level_prs(2)

    @property
    def toplevel(self):
        return self.__level_prs(1)

    @property
    def port(self):
        return self.obj.port

    @property
    def path(self):
        res = self.obj.path
        return res if res else None

    @property
    def query(self):
        res = self.obj.query
        return res if res else None

    @property
    def argument(self):
        return self.query

    @property
    def fragment(self):
        res = self.obj.fragment
        return res if res else None
