from io import BytesIO

from requests import Session
from requests.exceptions import HTTPError, RequestException
from urllib3 import disable_warnings
from urllib3.exceptions import InsecureRequestWarning
from user_agent import generate_user_agent

from spiderpig.crawl.loc import Location
from spiderpig.crawl.resp import Resp
from spiderpig.fixed import REQ_CHUNKSIZE, REQ_MAXSIZE, REQ_TIMEOUT


class Pull:
    session = None
    agent = None

    def __init__(
            self,
            verify_ssl: bool = False,
            timeout: int = None,
            maxsize: int = None,
    ):
        self.new_session(verify_ssl)
        self.new_agent()
        self.timeout = timeout if timeout else REQ_TIMEOUT
        self.maxsize = maxsize if maxsize else REQ_MAXSIZE

    @staticmethod
    def get_session():
        return Session()

    def new_session(self, verify_ssl: bool = False):
        self.session = self.get_session()
        if not verify_ssl:
            disable_warnings(InsecureRequestWarning)
        self.session.verify = verify_ssl

    @staticmethod
    def get_agent(device_type: str = 'desktop'):
        return generate_user_agent(device_type=device_type)

    def new_agent(self, **kwargs):
        self.agent = self.get_agent(**kwargs)

    @property
    def headers(self):
        return {'User-Agent': self.agent}

    def run(self, loc: Location):
        url = loc()
        status = 0
        bbuff = BytesIO()
        error = None

        with self.session as sess:
            try:
                response = sess.get(
                    url=url,
                    stream=True,
                    headers=self.headers,
                    timeout=self.timeout,
                )
            except RequestException as ex:
                error = str(ex)
            else:
                url = response.url
                status = response.status_code

                try:
                    response.raise_for_status()
                except HTTPError as ex:
                    error = str(ex)
                else:

                    length = 0
                    for chunk in response.iter_content(REQ_CHUNKSIZE):

                        length += bbuff.write(chunk)
                        if length > self.maxsize:
                            status = 0
                            error = 'Response too large'
                            break

        return Resp(
            loc=Location(url),
            status=status,
            content=bbuff.getvalue(),
            error=error
        )
