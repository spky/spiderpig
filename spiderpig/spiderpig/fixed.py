APP_NAME = 'Spider-Pig'
APP_SUBTITLE = 'Does whatever a {} does'.format(APP_NAME)

SECRET_KEY_NAME = 'secret.key'
SQLITE_DEV_NAME = 'database.dev.sqlite'
SQLITE_PROD_NAME = 'database.sqlite'


ERROR_CODES = (
    400, 401, 403, 404, 418,
    500, 501, 502, 503, 504
)

TRUTHY = ('true', 'on', '1')

REQ_TIMEOUT = 10
REQ_CHUNKSIZE = 1024
REQ_MAXSIZE = 4 * (1024 * REQ_CHUNKSIZE)

FORM_CRAWL_TTL_MIN = 1
FORM_CRAWL_TTL_MAX = 12
FORM_CRAWL_TTL_DEFAULT = 2

FORM_REGISTER_PASSWD_MIN = 8

PAGE_USER_NUM = 10
PAGE_CRAWL_NUM = 10
PAGE_URL_NUM = 25

LATEST_USER_NUM = 10
LATEST_CRAWL_NUM = 10
LATEST_URL_NUM = 15

FEED_USER_NUM = 10
FEED_CRAWL_NUM = 20
FEED_URL_NUM = 40

TRUNCATE = 64

USER_ICONS = (
    'fa-beer',
    'fa-bomb',
    'fa-bone',
    'fa-broom',
    'fa-carrot',
    'fa-coffee',
    'fa-crow',
    'fa-dragon',
    'fa-drumstick-bite',
    'fa-ghost',
    'fa-guitar',
    'fa-hammer',
    'fa-highlighter',
    'fa-hippo',
    'fa-horse-head',
    'fa-hotdog',
    'fa-kiwi-bird',
    'fa-magic',
    'fa-oil-can',
    'fa-poop',
    'fa-robot',
    'fa-skull',
    'fa-syringe',
    'fa-user-secret',
)
