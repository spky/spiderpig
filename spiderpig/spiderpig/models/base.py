from spiderpig.base import DB
from spiderpig.models.mixin_base import CreatedMixin, CRUDMixin, PrimaryMixin
from spiderpig.models.mixin_urls import ValueIntMixin, ValueStrMixin


class Model(CreatedMixin, PrimaryMixin, CRUDMixin, DB.Model):
    __table_args__ = {'extend_existing': True}
    __abstract__ = True


class UrlIntModel(ValueIntMixin, Model):
    __abstract__ = True


class UrlStrModel(ValueStrMixin, Model):
    __abstract__ = True


def db_pri_col(name: str, nullable: bool = True):
    name = '{}.prime'.format(name)
    return DB.Column(DB.Integer(), DB.ForeignKey(name), nullable=nullable)


def db_ref_rel(tbl: str, bref: str, *, self_rel: bool = False):
    return DB.relationship(
        tbl, backref=bref, lazy='joined',
        remote_side='{}.prime'.format(tbl) if self_rel else None,
        order_by='{}.created.desc()'.format(tbl),
    )
