from spiderpig.base import DB
from spiderpig.models.base import Model, db_pri_col, db_ref_rel


class Crawl(Model):
    __tablename__ = 'crawl'

    ttl = DB.Column(DB.Integer(), nullable=False, default=0)
    user_prime = db_pri_col('user', False)
    urls = db_ref_rel('Url', 'crawl')
