from sqlalchemy.ext.hybrid import hybrid_property

from spiderpig.crawl.loc import Location
from spiderpig.models.base import (
    Model, UrlIntModel, UrlStrModel, db_pri_col, db_ref_rel,
)


class Url(Model):
    __tablename__ = 'url'

    parent_prime = db_pri_col('url')
    parent = db_ref_rel('Url', 'children', self_rel=True)

    crawl_prime = db_pri_col('crawl', False)
    ttlhop_prime = db_pri_col('url_ttlhop', False)

    status_prime = db_pri_col('url_status', False)
    error_prime = db_pri_col('url_error')

    scheme_prime = db_pri_col('url_scheme')
    username_prime = db_pri_col('url_username')
    password_prime = db_pri_col('url_password')
    thirdlevel_prime = db_pri_col('url_thirdlevel')
    secondlevel_prime = db_pri_col('url_secondlevel')
    toplevel_prime = db_pri_col('url_toplevel')
    port_prime = db_pri_col('url_port')
    path_prime = db_pri_col('url_path')
    argument_prime = db_pri_col('url_argument')
    fragment_prime = db_pri_col('url_fragment')

    @classmethod
    def query_filter_good(cls, query=None):
        query = query if query else cls.query
        return query.join(UrlStatus).filter(UrlStatus.good.is_(True))

    @classmethod
    def query_filter_fail(cls, query=None):
        query = query if query else cls.query
        return query.join(UrlStatus).filter(UrlStatus.good.is_(False))

    @property
    def hop(self):
        return self.crawl.ttl - self.ttlhop.value

    @property
    def netloc(self):
        res = '.'.join(nl.value for nl in (
            self.thirdlevel, self.secondlevel, self.toplevel
        ) if nl)

        if self.port:
            res = ':'.join((res, str(self.port.value)))

        cred = ':'.join(cr.value for cr in (
            self.username, self.password
        ) if cr)
        if cred:
            res = '@'.join((cred, res))

        return res

    @property
    def address(self):
        loc = self()
        return loc()

    def __call__(self):
        return Location.generate((
            self.scheme.value if self.scheme else '',
            self.netloc,
            self.path.value if self.path else '',
            self.argument.value if self.argument else '',
            self.fragment.value if self.fragment else '',
        ))

    def __repr__(self):
        return 'Url({} - address="{}" error="{}")'.format(
            self.status.value, self.address,
            self.error.value if self.error else ''
        )

    def view(self):
        return {
            'address': self.address,
            'status': self.status.value,
            'error': self.error.value if self.error else None
        }


class UrlStatus(UrlIntModel):
    __tablename__ = 'url_status'

    urls = db_ref_rel('Url', 'status')

    @hybrid_property
    def good(self):
        stat = self.value
        return (stat >= 200) and (stat < 300)

    @good.expression
    def good(cls):
        stat = cls.value
        return (stat >= 200) & (stat < 300)


class UrlTtlHop(UrlIntModel):
    __tablename__ = 'url_ttlhop'

    urls = db_ref_rel('Url', 'ttlhop')


class UrlError(UrlStrModel):
    __tablename__ = 'url_error'

    urls = db_ref_rel('Url', 'error')


class UrlScheme(UrlStrModel):
    __tablename__ = 'url_scheme'

    urls = db_ref_rel('Url', 'scheme')


class UrlUsername(UrlStrModel):
    ___tablename__ = 'url_username'

    urls = db_ref_rel('Url', 'username')


class UrlPassword(UrlStrModel):
    __tablename__ = 'url_password'

    urls = db_ref_rel('Url', 'password')


class UrlThirdLevel(UrlStrModel):
    __tablename__ = 'url_thirdlevel'

    urls = db_ref_rel('Url', 'thirdlevel')


class UrlSecondLevel(UrlStrModel):
    __tablename__ = 'url_secondlevel'

    urls = db_ref_rel('Url', 'secondlevel')


class UrlTopLevel(UrlStrModel):
    __tablename__ = 'url_toplevel'

    urls = db_ref_rel('Url', 'toplevel')


class UrlPort(UrlIntModel):
    __tablename__ = 'url_port'

    urls = db_ref_rel('Url', 'port')


class UrlPath(UrlStrModel):
    __tablename__ = 'url_path'

    urls = db_ref_rel('Url', 'path')


class UrlArgument(UrlStrModel):
    __tablename__ = 'url_argument'

    urls = db_ref_rel('Url', 'argument')


class UrlFragment(UrlStrModel):
    __tablename__ = 'url_fragment'

    urls = db_ref_rel('Url', 'fragment')
