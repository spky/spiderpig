from datetime import datetime

from werkzeug.security import check_password_hash, generate_password_hash

from flask_login import UserMixin
from spiderpig.base import DB
from spiderpig.models.base import Model, db_ref_rel


class User(UserMixin, Model):
    __tablename__ = 'user'

    username = DB.Column(DB.String(), nullable=False)
    password = DB.Column(DB.String(), nullable=False)
    last_login = DB.Column(DB.DateTime(), nullable=True)
    icon = DB.Column(DB.String())
    crawls = db_ref_rel('Crawl', 'user')

    def __repr__(self):
        return 'User(name="{}")'.format(self.username)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def set_password(self, password, _commit=True):
        return self.update(
            password=generate_password_hash(password),
            _commit=_commit
        )

    def set_last_login(self, _commit=True):
        return self.update(
            last_login=datetime.utcnow(),
            _commit=_commit
        )

    @classmethod
    def query_filter_user(cls, username, query=None):
        query = query if query else cls.query
        return query.filter_by(username=username)

    @classmethod
    def query_first_user(cls, username, query=None):
        return cls.query_filter_user(username, query=query).first()

    def get_id(self):
        '''overwrite UserMixin.get_id()'''
        return str(self.prime)
