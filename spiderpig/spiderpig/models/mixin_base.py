from datetime import datetime

from sqlalchemy.sql import desc

from spiderpig.base import DB


class PrimaryMixin:
    prime = DB.Column(DB.Integer(), primary_key=True)

    @classmethod
    def by_prime(cls, value):
        if any([
                isinstance(value, (str, bytes)) and value.isdigit(),
                isinstance(value, (int, float))
        ]):
            return cls.query.get(int(value))
        return None


class CreatedMixin:

    created = DB.Column(
        DB.DateTime(), nullable=False, default=datetime.utcnow
    )

    @classmethod
    def query_order_created(cls, descending=True, query=None):
        query = query if query else cls.query
        return query.order_by(
            desc(cls.created) if descending else cls.created
        )


class CRUDMixin:

    @classmethod
    def create(cls, _commit=True, **kwargs):
        inst = cls(**kwargs)
        return inst.save(_commit=_commit)

    def update(self, _commit=True, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        if _commit:
            return self.save(_commit=_commit)
        return self

    def save(self, _commit=True):
        DB.session.add(self)
        if _commit:
            DB.session.commit()
        return self

    def delete(self, _commit=True):
        DB.session.delete(self)
        if _commit:
            DB.session.commit()
        return True
