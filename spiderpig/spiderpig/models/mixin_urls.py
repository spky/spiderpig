from spiderpig.base import DB


class ValueQueryMixin:

    @classmethod
    def query_filter_value(cls, value, query=None):
        query = query if query else cls.query
        return query.filter_by(value=value)

    @classmethod
    def query_first_value(cls, value, query=None):
        return cls.query_filter_value(value, query=query).first()


class ValueIntMixin(ValueQueryMixin):
    value = DB.Column(DB.Integer(), nullable=False)


class ValueStrMixin(ValueQueryMixin):
    value = DB.Column(DB.String(), nullable=False)
