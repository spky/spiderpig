from flask import Blueprint, render_template

from spiderpig.fixed import (
    LATEST_CRAWL_NUM, LATEST_URL_NUM, LATEST_USER_NUM, PAGE_CRAWL_NUM,
    PAGE_URL_NUM, PAGE_USER_NUM,
)
from spiderpig.models.crawl import Crawl
from spiderpig.models.url import Url
from spiderpig.models.user import User

BLUEPRINT_MAIN = Blueprint('main', __name__)


@BLUEPRINT_MAIN.route('/')
def index():

    num_url = Url.query.count()
    num_url_fail = Url.query_filter_fail().count()
    num_url_good = (num_url - num_url_fail)

    return render_template(
        'main/index.html',
        num_user=User.query.count(),
        num_crawl=Crawl.query.count(),
        num_url=num_url,
        num_url_fail=num_url_fail,
        num_url_good=num_url_good,
        lat_user=User.query_order_created().limit(LATEST_USER_NUM).all(),
        lat_crawl=Crawl.query_order_created().limit(LATEST_CRAWL_NUM).all(),
        lat_url=Url.query_order_created().limit(LATEST_URL_NUM).all(),
    )


@BLUEPRINT_MAIN.route('/users/<int:page>')
@BLUEPRINT_MAIN.route('/users/')
@BLUEPRINT_MAIN.route('/users')
def users(page=None):

    return render_template(
        'main/user.html',
        title='Users',
        users=User.query_order_created().paginate(
            page, per_page=PAGE_USER_NUM
        ),
    )


@BLUEPRINT_MAIN.route('/user/<int:prime>')
def user_single(prime):
    user = User.query.get_or_404(prime)

    return render_template(
        'main/user.html',
        title='{} User'.format(user.username),
        user=user,
    )


@BLUEPRINT_MAIN.route('/crawls/<int:page>')
@BLUEPRINT_MAIN.route('/crawls/')
@BLUEPRINT_MAIN.route('/crawls')
def crawls(page=None):

    return render_template(
        'main/crawl.html',
        title='Crawls',
        crawls=Crawl.query_order_created().paginate(
            page, per_page=PAGE_CRAWL_NUM
        ),
    )


@BLUEPRINT_MAIN.route('/crawl/<int:prime>')
def crawl_single(prime):
    crawl = Crawl.query.get_or_404(prime)

    return render_template(
        'main/crawl.html',
        title='Crawl details',
        crawl=crawl,
    )


@BLUEPRINT_MAIN.route('/urls/<int:page>')
@BLUEPRINT_MAIN.route('/urls/')
@BLUEPRINT_MAIN.route('/urls')
def urls(page=None):

    return render_template(
        'main/url.html',
        title='URLs',
        urls=Url.query_order_created().paginate(
            page, per_page=PAGE_URL_NUM
        ),
    )


@BLUEPRINT_MAIN.route('/url/<int:prime>')
def url_single(prime):
    url = Url.query.get_or_404(prime)

    return render_template(
        'main/url.html',
        title='URL details',
        url=url,
    )


@BLUEPRINT_MAIN.route('/urls/good/<int:page>')
@BLUEPRINT_MAIN.route('/urls/good/')
@BLUEPRINT_MAIN.route('/urls/good')
def urls_good(page=None):

    return render_template(
        'main/url.html',
        title='Good URLs',
        urls=Url.query_filter_good(
            query=Url.query_order_created()
        ).paginate(
            page, per_page=PAGE_URL_NUM
        )
    )


@BLUEPRINT_MAIN.route('/urls/fail/<int:page>')
@BLUEPRINT_MAIN.route('/urls/fail/')
@BLUEPRINT_MAIN.route('/urls/fail')
def urls_fail(page=None):

    return render_template(
        'main/url.html',
        title='Failed URLs',
        urls=Url.query_filter_fail(
            query=Url.query_order_created()
        ).paginate(
            page, per_page=PAGE_URL_NUM
        )
    )
