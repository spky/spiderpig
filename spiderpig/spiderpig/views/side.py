from flask import Blueprint, current_app, render_template, request, url_for
from werkzeug.contrib.atom import AtomFeed, FeedEntry

from spiderpig.fixed import FEED_CRAWL_NUM, FEED_URL_NUM, FEED_USER_NUM
from spiderpig.forms.find import FindForm
from spiderpig.models.crawl import Crawl
from spiderpig.models.url import Url
from spiderpig.models.user import User

BLUEPRINT_SIDE = Blueprint('side', __name__)


@BLUEPRINT_SIDE.route('/about')
def about():

    return render_template(
        'side/about.html',
        title='About',
    )


@BLUEPRINT_SIDE.route('/find', methods=['GET', 'POST'])
def find():
    def _search(query):
        for url in Url.query_order_created().all():
            if any((
                    query in str(url.status.value),
                    url.error and query in url.error.value,
                    query in url.address
            )):
                yield url

    query = request.args.get('q') or request.form.get('query')
    if query:
        query = query.strip()
    results = None
    form = FindForm(query=query)

    if query:
        results = list(_search(query))

    return render_template(
        'side/find.html',
        title='Search',
        form=form,
        query=query,
        results=results,
    )


@BLUEPRINT_SIDE.route('/feed.xml')
def feed():
    def _entries():
        def _entry(*, title, content, endpoint, obj, user: User):
            return FeedEntry(
                title=title, title_type='text',
                content=content, content_type='html',
                url=url_for(endpoint, prime=obj.prime, _external=True),
                author={
                    'name': user.username,
                    'uri': url_for(
                        'main.user_single', prime=user.prime, _external=True
                    ),
                },
                published=obj.created,
                updated=obj.created,
            )

        return (fl for at in (

            (_entry(
                title='User #{} ({})'.format(user.prime, user.username),
                content='''
<dl>
    <dt>User</dt>
    <dd>#{prime} ({username})</dd>
    <dt>Crawls</dt>
    <dd>{crawls}</dd>
</dl>
                '''.strip().format(
                    prime=user.prime,
                    username=user.username,
                    crawls=len(user.crawls),
                ),
                endpoint='main.user_single',
                obj=user, user=user,
            ) for user in User.query_order_created().limit(
                FEED_USER_NUM
            ).all()),

            (_entry(
                title='Crawl #{}'.format(crawl.prime),
                content='''
<dl>
    <dt>Crawl</dt>
    <dd>#{prime}</dd>
    <dt>Urls</dt>
    <dd>{urls}</dd>
    <dt>User</dt>
    <dd>#{user_prime} ({username})</dd>
</dl>
                '''.strip().format(
                    prime=crawl.prime,
                    urls=len(crawl.urls),
                    user_prime=crawl.user.prime,
                    username=crawl.user.username,
                ),
                endpoint='main.crawl_single',
                obj=crawl, user=crawl.user,
            ) for crawl in Crawl.query_order_created().limit(
                FEED_CRAWL_NUM
            ).all()),

            (_entry(
                title='URL #{}'.format(url.prime),
                content='''
<dl>
    <dt>URL</dt>
    <dd>#{prime}</dd>
    <dt>Address</dt>
    <dd><a href="{address}">{address}</a><dd>
    <dt>Status</dt>
    <dd>{status}</dd>
    <dt>Error</dt>
    <dd><code>{error}</code></dd>
    <dt>Crawl</dt>
    <dd>#{crawl_prime}</dd>
    <dt>User</dt>
    <dd>#{user_prime} ({username})</dd>
</dl>
                '''.strip().format(
                    prime=url.prime,
                    address=url.address,
                    status=url.status.value,
                    error=url.error.value if url.error else '-',
                    crawl_prime=url.crawl.prime,
                    user_prime=url.crawl.user.prime,
                    username=url.crawl.user.username,
                ),
                endpoint='main.url_single',
                obj=url, user=url.crawl.user,
            ) for url in Url.query_order_created().limit(
                FEED_URL_NUM
            ).all()),

        ) for fl in at)

    app_name = current_app.config['APP_NAME']
    app_subtitle = current_app.config['APP_SUBTITLE']
    url = url_for('main.index', _external=True)

    return AtomFeed(
        title='{} News Feed'.format(app_name), title_type='text',
        subtitle=app_subtitle, subtitle_type='text',
        url=url, feed_url=url_for('side.feed', _external=True),
        generator=(app_name, url, 'NaN'),
        entries=sorted(
            _entries(), reverse=True,
            key=lambda entry: entry.updated,
        ),
    ).get_response()
