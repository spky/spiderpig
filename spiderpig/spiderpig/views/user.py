from flask import (
    Blueprint, abort, current_app, flash, redirect, render_template, request,
    url_for,
)

from flask_login import current_user, login_required, login_user, logout_user
from spiderpig.base import LMAN
from spiderpig.forms.user import ChangeForm, LoginForm, RegisterForm
from spiderpig.models.user import User

BLUEPRINT_USER = Blueprint('user', __name__, url_prefix='/user')


@LMAN.user_loader
def load_user(user_prime):
    return User.by_prime(user_prime)


def redirect_user_next():
    if not current_user.is_authenticated:
        return None
    redir = request.args.get('next')
    return redirect(redir if redir else url_for('user.home'))


@BLUEPRINT_USER.route('/logout')
@login_required
def logout():
    logout_user()
    flash('See you soon!', 'is-success')
    return redirect(url_for('main.index'))


@BLUEPRINT_USER.route('/login', methods=['GET', 'POST'])
def login():
    redir = redirect_user_next()
    if redir:
        return redir

    form = LoginForm()

    if form.validate_on_submit():
        user = form.retrieve()
        if user:
            flash('Welcome {}!'.format(user.username), 'is-success')
            login_user(user, remember=form.remember.data)
            redir = redirect_user_next()
            if redir:
                return redir

    return render_template(
        'user/handle.html',
        title='Login',
        form=form,
    )


@BLUEPRINT_USER.route('/register', methods=['GET', 'POST'])
def register():
    redir = redirect_user_next()
    if redir:
        return redir

    if not current_app.config['OPEN_REGISTRATION']:
        abort(404)

    form = RegisterForm()

    if form.validate_on_submit():
        user = form.retrieve()
        if user:
            flash('Hello {}! You are new here!'.format(user.username))
            login_user(user, remember=False)
            redir = redirect_user_next()
            if redir:
                return redir

    return render_template(
        'user/handle.html',
        title='Register',
        form=form,
    )


@BLUEPRINT_USER.route('/change', methods=['GET', 'POST'])
@login_required
def change():
    user = User.by_prime(current_user.prime)

    form = ChangeForm(icon=user.icon)

    if form.validate_on_submit():
        if form.retrieve(user):
            flash('Changed!')
            redir = redirect_user_next()
            if redir:
                return redir

    return render_template(
        'user/change.html',
        title='Change',
        form=form,
    )

@BLUEPRINT_USER.route('/home')
@login_required
def home():
    user = User.by_prime(current_user.prime)

    return render_template(
        'user/home.html',
        title='{} Home'.format(user.username),
        user=user,
    )
