from flask import (
    Blueprint, Response, abort, render_template, request, stream_with_context,
)
from flask.json import dumps

from flask_login import current_user, login_required
from spiderpig.crawl.spider import Spider
from spiderpig.fixed import FORM_CRAWL_TTL_DEFAULT
from spiderpig.forms.crawl import SnapForm

BLUEPRINT_CRAWL = Blueprint('crawl', __name__, url_prefix='/crawl')


@BLUEPRINT_CRAWL.route('/', methods=['GET', 'POST'])
@login_required
def index():
    form = SnapForm(
        address=request.args.get('address'),
        ttl=request.args.get('ttl', FORM_CRAWL_TTL_DEFAULT),
    )
    submitted = form.validate_on_submit()

    return render_template(
        'crawl/index.html',
        title='Crawl',
        form=form,
        submitted=submitted,
    )


@BLUEPRINT_CRAWL.route('/snap', methods=['POST'])
@login_required
def snap():
    address = request.values.get('address', None)
    ttl = request.values.get('ttl', None)
    if not address or not ttl:
        abort(400)
    try:
        ttl = int(ttl)
    except ValueError:
        abort(400)

    spider = Spider()
    def collect():
        for elem in spider(address=address, ttl=ttl, user=current_user):
            yield '{}\n\t\t\n'.format(dumps(elem.view()))

    return Response(
        stream_with_context(collect()),
        status=200,
        mimetype='text/plain'
    )
