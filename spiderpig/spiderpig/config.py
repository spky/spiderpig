from os import getenv, path, urandom

from spiderpig.fixed import (
    APP_NAME, APP_SUBTITLE, SECRET_KEY_NAME, SQLITE_DEV_NAME, SQLITE_PROD_NAME,
    TRUNCATE, TRUTHY,
)

# pylint: disable=too-few-public-methods

ROOT_DIR = path.abspath(path.dirname(__file__))
BASE_DIR = path.abspath(path.join(ROOT_DIR, path.pardir))
DATA_DIR = path.abspath(path.join(BASE_DIR, getenv('DATA_DIR', 'data')))


def _secret_key():
    location = path.join(DATA_DIR, SECRET_KEY_NAME)
    if not path.exists(location):
        secret = urandom(512)
        with open(location, 'wb') as opn:
            opn.write(secret)
        return secret
    with open(location, 'rb') as opn:
        return opn.read()


class Config:
    APP_DIR = ROOT_DIR
    APP_NAME = APP_NAME
    APP_SUBTITLE = APP_SUBTITLE
    DEBUG = False
    TESTING = False
    TRUNCATE = TRUNCATE
    OPEN_REGISTRATION = (getenv('OPEN_REGISTRATION', '').lower() in TRUTHY)
    SECRET_KEY = _secret_key()
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(
        path.join(DATA_DIR, SQLITE_DEV_NAME)
    )


class ProdConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(
        path.join(DATA_DIR, SQLITE_PROD_NAME)
    )
