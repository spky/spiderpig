from random import choice

from wtforms import (
    BooleanField, PasswordField, SelectField, StringField, SubmitField,
)
from wtforms.validators import (
    EqualTo, InputRequired, Length, Optional, ValidationError,
)

from flask_wtf import FlaskForm
from spiderpig.fixed import FORM_REGISTER_PASSWD_MIN, USER_ICONS
from spiderpig.models.user import User


def single_username(_, field):
    username = field.data
    if User.query_first_user(username):
        raise ValidationError(
            'User {} already present'.format(username)
        )


def existing_username(_, field):
    username = field.data
    if not User.query_first_user(username):
        raise ValidationError(
            'There is no one by the name of {}'.format(username)
        )


def non_integer_username(_, field):
    username = field.data
    try:
        int(username)
    except ValueError:
        return
    raise ValidationError(
        'Username can\'t be a number'
    )


class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[
        InputRequired(),
        non_integer_username,
        single_username,
    ], description='Do you have a name?')
    password = PasswordField('Password', validators=[
        InputRequired(),
        Length(
            min=FORM_REGISTER_PASSWD_MIN,
            message='Password should at least have %(min)s characters',
        ),
        EqualTo('confirm', message='Passwords must match'),
    ], description='Choose wisely!')
    confirm = PasswordField('Password confirm', validators=[
        InputRequired(),
        EqualTo('password', message='Passwords must match'),
    ], description='Do not repeat yourself!')

    submit = SubmitField('Submit')

    def retrieve(self):
        username = self.username.data
        password = self.password.data
        if User.query_first_user(username):
            return None

        user = User.create(
            username=username,
            icon=choice(USER_ICONS),
            _commit=False
        )
        user.set_password(password=password, _commit=False)
        user.set_last_login(_commit=True)
        return user


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[
        InputRequired(), existing_username,
    ], description='Do I know you?')
    password = PasswordField('Password', validators=[
        InputRequired(),
    ], description='I won\'t tell anyone - promised!')
    remember = BooleanField('Remember me')

    submit = SubmitField('Submit')

    def retrieve(self):
        username = self.username.data
        password = self.password.data
        user = User.query_first_user(username)
        if not user:
            return None
        if not user.check_password(password):
            return None
        user.set_last_login(_commit=True)
        return user


class ChangeForm(FlaskForm):
    password = PasswordField('Password', validators=[
        Optional(),
        Length(
            min=FORM_REGISTER_PASSWD_MIN,
            message='Password should at least have %(min)s characters',
        ),
        EqualTo('confirm', message='Passwords must match'),
    ], description='Leave blank to keep the old one!')
    confirm = PasswordField('Password confirm', validators=[
        Optional(),
        EqualTo('password', message='Passwords must match'),
    ], description='Do not repeat yourself!')

    icon = SelectField(
        'Icon', coerce=str, choices=[
            (ico, ' '.join(
                ic.capitalize() for ic in ico.lstrip('fa-').split('-')
            )) for ico in USER_ICONS
        ], render_kw={'class': 'dynamic-icon'}, description='Please be kind.'
    )

    submit = SubmitField('Submit')

    def retrieve(self, user):
        password = self.password.data
        if password.strip():
            user.set_password(password=password, _commit=False)

        user.update(icon=self.icon.data, _commit=True)

        return user
