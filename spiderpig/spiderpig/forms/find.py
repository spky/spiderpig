from wtforms import StringField, SubmitField
from wtforms.validators import InputRequired

from flask_wtf import FlaskForm


class FindForm(FlaskForm):
    query = StringField('Query', validators=[
        InputRequired(),
    ], description='Tell me what you are looking for!')

    submit = SubmitField('Search')
