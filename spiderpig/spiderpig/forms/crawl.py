from wtforms import IntegerField, StringField, SubmitField
from wtforms.validators import URL, InputRequired, NumberRange

from flask_wtf import FlaskForm
from spiderpig.fixed import FORM_CRAWL_TTL_MAX, FORM_CRAWL_TTL_MIN


class SnapForm(FlaskForm):
    address = StringField('Address', validators=[
        InputRequired(),
        URL(message='This does not look like an URL')
    ], description='Where should I start?')
    ttl = IntegerField('Time to live', validators=[
        InputRequired(),
        NumberRange(
            min=FORM_CRAWL_TTL_MIN, max=FORM_CRAWL_TTL_MAX,
            message='Should be between %(min)s and %(max)s',
        )
    ], description='How far should I go?')

    submit = SubmitField('Submit')
