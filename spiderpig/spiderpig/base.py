from flask_sqlalchemy import SQLAlchemy

from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect

DB = SQLAlchemy()
CSRF = CSRFProtect()
LMAN = LoginManager()
