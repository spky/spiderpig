; const ctrl = (function () {
  'use strict';
  // progress bar handler
  function progressBar(barId) {
    let max = 0, val = 0; const step = 1, span = 10;
    const elem = document.getElementById(barId);
    function barSet() { if (!!elem) { elem.max = max; elem.value = val; } }
    barSet();
    return {
      reset: function () { max = 1; val = 0; barSet(); },
      finish: function () { val = max; barSet(); },
      step: function () {
        val += step; if (max - val <= span) { max += span; } barSet();
      },
    };
  }
  // snap status display
  function resultCard(target, jObj) {
    // html element creators
    function create(tag, classes) {
      const res = document.createElement(tag);
      if (!!classes) { res.setAttribute('class', classes); }
      return res
    }
    function make(parent, tag, classes) {
      const res = create(tag, classes);
      parent.appendChild(res);
      return res;
    }
    const content = create('div', 'content');
    const card = make(content, 'div', 'card');
    const cHeader = make(card, 'header', 'card-header');
    const cHTitle = make(cHeader, 'div', 'card-header-title');
    cHTitle.innerText = jObj.status;
    const cHIcon = make(cHeader, 'div', 'card-header-icon');
    if (!!jObj.status && jObj.status >= 200 && jObj.status < 300) {
      make(
        make(cHIcon, 'span', 'icon has-text-success'),
        'i', 'fas fa-award'
      );
    } else {
      make(
        make(cHIcon, 'span', 'icon has-text-danger'),
        'i', 'fas fa-fire-alt'
      );
    }
    if (!!jObj.error) {
      make(
        make(
          make(card, 'div', 'card-content'),
          'div', 'content has-text-centered'
        ),
        'code'
      ).innerText = jObj.error;
    }
    if (!!jObj.address) {
      const link = make(
        make(card, 'footer', 'card-footer'),
        'a', 'card-footer-item'
      );
      link.setAttribute('href', jObj.address);
      link.innerText = jObj.address;
    }
    target.insertBefore(content, target.firstChild);
  }
  // snap status main
  return function (endpoint, address, ttl, csrf, contentId, barId) {
    const separator = '\n\t\t\n';
    const bar = progressBar(barId);
    const container = document.getElementById(contentId);
    const req = new XMLHttpRequest();
    req.open('POST', endpoint);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.setRequestHeader('X-CSRFToken', csrf);
    req.send('address=' + address + '&ttl=' + ttl);
    req.onerror = function () { console.error(this); };
    let position = 0;
    function handle() {
      const messages = req.responseText.split(separator);
      messages.slice(position, -1).forEach(function (value) {
        resultCard(container, JSON.parse(value));
        bar.step();
      });
      position = messages.length - 1;
    }
    const timer = setInterval(function () {
      handle();
      if (req.readyState === XMLHttpRequest.DONE) {
        clearInterval(timer);
        bar.finish();
      }
    }, 500);
  };
})();
