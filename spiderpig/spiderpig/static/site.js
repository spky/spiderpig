; +(function () {
  'use strict';
  document.addEventListener("DOMContentLoaded", function () {
    // navigation toggle
    const navBurger = document.getElementById('navBurger');
    const navToggle = document.getElementById('navToggle');
    navBurger.addEventListener('click', function () {
      navToggle.classList.toggle('is-active');
      navBurger.classList.toggle('is-active');
    });
    // moment trigger
    document.querySelectorAll('.moment').forEach(function (elem) {
      const stamp = elem.getAttribute('data-stamp');
      if (!!stamp) { elem.textContent = moment.unix(stamp).fromNow(); }
    });
    // icon select box
    document.querySelectorAll('select.dynamic-icon').forEach(function (sdyc) {
      const cont = sdyc.parentElement;
      const ctrl = cont.parentElement;
      if (!!sdyc && !!ctrl) {
        cont.setAttribute('class', cont.getAttribute('class') + ' is-medium');
        ctrl.setAttribute('class', ctrl.getAttribute('class') + ' has-icons-left');
        const icnt = document.createElement('div');
        icnt.setAttribute('class', 'icon is-left');
        cont.appendChild(icnt);
        function refresh() {
          function trigger(name) {
            name = name || 'fa-spinner fa-pulse';
            const icon = document.createElement('i');
            icon.setAttribute('class', 'fas ' + name);
            while (icnt.childNodes.length) { icnt.firstChild.remove(); }
            icnt.appendChild(icon);
          }
          trigger();
          const curr = sdyc.querySelectorAll('option')[sdyc.selectedIndex]
          if (!!curr) { trigger(curr.value); }
        }
        sdyc.addEventListener('change', refresh);
        refresh();
      }
    });
  });
})();
