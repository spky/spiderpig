from os import getenv

from flask.helpers import get_debug_flag

from spiderpig.config import DevConfig, ProdConfig
from spiderpig.main import create_app

# pylint: disable=invalid-name


DEFAULT_HOST = 'localhost'
DEFAULT_PORT = 5000

app = create_app(
    DevConfig if get_debug_flag() else ProdConfig
)
wsgi_app = app.wsgi_app


def launch():
    host = getenv('SERVER_HOST', DEFAULT_HOST)
    port = getenv('SERVER_PORT', str(DEFAULT_PORT))

    port = int(port) if port.isdigit() else DEFAULT_PORT

    app.run(host, port, debug=True, threaded=True)


if __name__ == '__main__':
    launch()
