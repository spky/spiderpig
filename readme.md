# Spider-Pig

> Spider-Pig, Spider-Pig,

> Does whatever a Spider-Pig does.

> Can he swing from a web?

> No, he can't, he's a pig,

> Look out, he is a Spider-Pig!

<br><br>
<center>
![Spider Pig](spiderpig.png)
</center>

## Development

Guess it was a mistake to use VisualStudio for Python development.
But it's too late now.


### Virtual environment

This project uses a virtual environment below ``spiderpig/env``.

If you continue to use VisualStudio make sure you have all extensions
installed for python development. Then inside the solution click on
*Python Environments* and select *Add Virtual Environment*. Use the
foldername `env` and tick the option to restore from `requirements.txt`.

If you are using a more decent operating system then do something
like this:

```
cd /where/you/cloned/spiderpig.git/spiderpig

python3 -m venv env
source env/bin/activate

python3 -m pip install -r requirements.txt
```


## Launch

If you insist in using Wintendo use this to start the project in
PowerShell:

```
.\env\Scripts\activate
$env:FLASK_APP="run.py"
flask run
```

On a proper \*nix system use this:

```
source ./env/bin/activate
export FLASK_APP=run.py
flask run
```


## Deploy

All writable data will be created below the ``data`` folder.
Please create it beforehand and give correct write permissions to it.

For example on some \*nix system:

```
cd /where/you/cloned/spiderpig.git/spiderpig

mkdir data
chown www:www data
```
